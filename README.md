# Optical Detection of Heparin in Whole Blood Samples using  Nanosensors Embedded in an Agarose Hydrogel 

Here are available the codes used for both analysis methods in the paper "Optical Detection of Heparin in Whole Blood Samples using Nanosensors Embedded in an Agarose Hydrogel." They were created and used on Wolfram Mathematica 12.1. 
